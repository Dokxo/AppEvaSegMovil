﻿using System.IO;
using AppEvaSegMovil.Interfaces.SQLite;
using AppEvaSegMovil.UWP.Services.SQLite;
using Windows.Storage;
using Xamarin.Forms;

[assembly: Dependency(typeof(FicConfigSQLiteUWP))]
namespace AppEvaSegMovil.UWP.Services.SQLite
{
    class FicConfigSQLiteUWP : IFicConfigSQLiteNETStd
    {
        public string FicGetDatabasePath()
        {
            return Path.Combine(ApplicationData.Current.LocalFolder.Path, AppSettings.ficDatabaseName);
        }
    }
}