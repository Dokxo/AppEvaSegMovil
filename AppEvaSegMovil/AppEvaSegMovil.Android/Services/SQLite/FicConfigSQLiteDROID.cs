﻿using System.IO;
using Xamarin.Forms;
using AppEvaSegMovil.Interfaces.SQLite;
using AppEvaSegMovil.Droid.Services.SQLite;

[assembly: Dependency(typeof(ficConfigSQLiteDROID))]
namespace AppEvaSegMovil.Droid.Services.SQLite
{
    public class ficConfigSQLiteDROID : IFicConfigSQLiteNETStd

    {
        public string FicGetDatabasePath()
        {
            string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            return Path.Combine(path, AppSettings.ficDatabaseName);
        }
    }
}